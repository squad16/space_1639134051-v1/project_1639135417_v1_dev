from pyspark.sql import SparkSession, DataFrame
import click


def read_data(spark: SparkSession, snap_date) -> DataFrame:
    # TODO Change HIVE_TABLE_NAME to real hive table name
    return spark.read.table('HIVE_TABLE_NAME').where('')


@click.command()
@click.option('-d', '--snap-date', required=True, help='Snapshot date of data in input table')
@click.option('-u', '--username', required=True, help='Database username')
@click.option('-p', '--password', required=True, help='Database password')
def main(snap_date, username, password):
    spark = SparkSession.builder.enableHiveSupport().getOrCreate()
    properties = {
        'user': username,
        'password': password,
        'driver': 'oracle.jdbc.driver.OracleDriver'
    }
    df = read_data(spark, snap_date)
    # TODO change ORACLE_TABLE_NAME to real oracle table name
    df.write.mode('append').jdbc('jdbc:oracle:thin:@//dwx.megafon.ru:1521/bd_dwx', 'ORACLE_TABLE_NAME',
                                 properties=properties)


if __name__ == '__main__':
    main()
